# Die Voet
Die Voet (Afrikaans for the foot) is just a silly Slack integration app made for some office shenanigans. Our CTO injured his foot just before going away on vacation, and all the photos had his foot in a cast in it. So I decided to turn it into an add-on where we can add his foot into any image.

When someone reacts to an image using our :die_voet:, our little Raspberry Pi server, using ngrok, receives the event from the Slack API, downloads the image, adds Die Voet, and then posts it back to the channel, leading to some hilarious results :)

![](images/image1.png)
![](images/image2.png)
![](images/image3.png)

The code is dirty and untidy but if someone is curious how to use a basic http server in python or how to integrate with Slack, you're welcome to take a look :)