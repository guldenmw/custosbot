#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
from PIL import Image
import json
import requests
from urlparse import urlparse, parse_qs
import urllib
from cStringIO import StringIO
from io import BytesIO

from env import CLIENT_ID, CLIENT_SECRET, VERIFICATION_TOKEN, BEARER_TOKEN, USER_TOKEN


PORT_NUMBER = 8080

list_of_files = []

def fetch_image(url):
    res = requests.get(url, headers={"Authorization": "Bearer {}".format(USER_TOKEN)})

    if res.status_code == 200:
        return res.content

    #file = urllib.URLopener()
    #file.retrieve(url, "bakcground.png")

def upload(og_file, new_file, channel):
    new_file.save("test_file.png", 'PNG', quality=70)
    my_file = {
      'file' : ('./test_file.png', open('./test_file.png', 'rb'), 'png')
    }

    options = {
        "token": BEARER_TOKEN,
        "filename": "die_voet_was_by_{}.png".format(og_file["name"]),
        "title": "Die voet was hier!",
        "channels": channel
    }

    res = requests.post('https://slack.com/api/files.upload', data=options, files=my_file)

    print(res.content)

    return "ok"


def fetch_and_add_die_voet(file_id):
    print(file_id)
    options = {
        "token": USER_TOKEN,
        "file": file_id
    }
    response = requests.get('https://slack.com/api/files.info', params=options)

    file = json.loads(response.content)["file"]
    print(file)

    filename = file["timestamp"]

    if file["user"] != "UBFCWBP9Q":

        if file["filetype"] == "jpg" or file["filetype"] == "png":
            background = Image.open(StringIO(fetch_image(file["url_private"])))

            die_voet = Image.open("die_voet.png")

            new_height = int(background.size[1]*0.6)
            new_width = int(die_voet.size[0]*1.0/die_voet.size[1] * new_height)
            die_voet_resize = die_voet.resize((new_width, new_height), Image.ANTIALIAS)

            background.paste(die_voet_resize, (0, background.size[1] - new_height), die_voet_resize)

            file_id = upload(file, background, file["channels"][0])



#This class will handles any incoming request from
#the browser 
class myHandler(BaseHTTPRequestHandler):
	#Handler for the GET requests
    def do_GET(self):
        path = urlparse(self.path).path
        if path == "/":
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()
            # Send the html message
            self.wfile.write("Hello World !")
            return

        if path == "/oauth":
            params = parse_qs(urlparse(self.path).query)
            #try:
            if params["code"][0]:
                options = {
                    'client_id': CLIENT_ID,
                    'client_secret': CLIENT_SECRET,
                    'code': params['code'][0]
                }

                r = requests.post("https://slack.com/api/oauth.access", data=options)

                print(r.content)
                self.send_response(200)
            #except:
            #    self.send_response(500)
            return


    def do_POST(self):
        path = urlparse(self.path).path
        if path == "/events":
            content_len = int(self.headers.getheader('content-length', 0))
            post_body = self.rfile.read(content_len)
            data = json.loads(post_body)

            if data["token"] != VERIFICATION_TOKEN:
                print("Incorrect verification token")
                self.send_response(400)
                return

            if data["type"] == "url_verification":
                response = {
                    'challenge': data["challenge"]
                }
                self.send_response(200)
                self.send_header('Content-type','application/json')
                self.end_headers()
                # Send the html message
                self.wfile.write(json.dumps(response))
                return

            if data["type"] == "event_callback":
                self.send_response(200)
                self.send_header('Content-type', 'test/html')
                self.end_headers()
                event = data["event"]

                if event["type"] == "reaction_added":
                    if event["reaction"] == "die_voet" and event["item"]["type"] == "file":
                        file_id = event["item"]["file"]
                        if not file_id in list_of_files:
                            list_of_files.append(file_id)
                            fetch_and_add_die_voet(file_id)


                #if event["subtype"] == "file_share":
                #    print(event["user"])
                #    if event["user"] != "UBFCWBP9Q":
                #        file = event["file"]
                #        if not file["name"] in list_of_files:
                #            list_of_files.append(file["name"])
                #            fetch_and_add_die_voet(file, event["channel"])
                return

try:
    #Create a web server and define the handler to manage the
    #incoming request
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print 'Started httpserver on port ' , PORT_NUMBER
    #Wait forever for incoming htto requests
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()
